import React from 'react'
import Carousel from 'react-bootstrap/Carousel';
import Cards from './Cards';
import { useNavigate } from 'react-router-dom';


function Home() {
  const navigate = useNavigate()
  const handleClick = () => {
    navigate('/nursery')
  }
  const products = () => {
    navigate('/products')
  }


  let items = [
    {
      Name: "Indoor Plants",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/Spathyphyllum_432x432.jpg?v=1637468992"
    },
    {
      Name: "NASA recommended Air Purifying Plants",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/002_2f7864b6-4c75-4713-8e5e-ca0714c39859_432x432.png?v=1631528610"
    },
    {
      Name: "Cactus & Succulents",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/002_10_432x432.jpg?v=1633236420"
    },
    {
      Name: "Medicinal Trees",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/Justiciaadhatoda_432x432.jpg?v=1608906478"
    },
    {
      Name: "Fruit Trees",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/004_3_c069fa9f-ca0d-451b-b1fd-6338eea6758d_432x432.png?v=1633842281"
    },
    {
      Name: "Outdoor Plants",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/Parijathcoraljasmine_432x432.jpg?v=1622831365"
    },
    {
      Name: "Ornamantal Trees",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/001_8e7f4be7-224f-403e-bf56-bf6a2a5129fb_432x432.png?v=1631507858"
    },
    {
      Name: "Flowering Trees",
      Img: "https://www.heartyculturenursery.com/cdn/shop/products/513A8963_432x432.png?v=1636172462"
    }
  ]
  return (
    <div>

      {/* Carousel */}
      <Carousel className='fixed'>
        <Carousel.Item >
          <img src="https://www.heartyculturenursery.com/cdn/shop/files/2_59458964-93df-41da-8a89-515add00e716_575x288_crop_center.jpg?v=1691581755" alt=""  className=''  style={{ width: '100%', height: '400px' }} />
          <Carousel.Caption className='text-center  fw-bolder'>
            <p className=' fs-1'>Buy Online: Greenery at your Fingertips</p>
            <p >Browse, select, and purchase your favorite plants from the comfort of your home.</p>
            <p className='btn btn-warning' onClick={products}>Order Now</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img src="https://www.heartyculturenursery.com/cdn/shop/files/217_575x288_crop_center.jpg?v=1691582998" alt="" style={{ width: '100%', height: '400px' }} />
          <Carousel.Caption><center>
            <p className='text-center fw-bolder fs-1'>Landscaping Service: Crafting Green Masterpieces</p>
            <p className='text-center fw-bolder fs-5 '>Tranform your outdoor areas with our bespoke landscaping services.</p></center>
            <p className='btn btn-warning' onClick={handleClick}>Know More</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img src="https://www.heartyculturenursery.com/cdn/shop/files/Untitled_design_2_575x288_crop_center.jpg?v=1691581377" alt="" style={{ width: '100%', height: '400px' }} />
          <Carousel.Caption className='text-primary'><center>
            <p className='text-center fw-bolder fs-1'>Our Nurtsery: Nurturing Nature's Beauty</p>
            <p className='fw-bolder'>Dive into our vast selection of thriving plants, each cultivated with care and expertise.</p></center>
            <p className='btn btn-warning' onClick={handleClick}>Know More</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      {/* Products */}
      <div className='mt-3'>
        <h4 className='text-info text-center'>Our Top Selling Plants & Trees</h4>
        <div className='container'>
          <div className='row mt-3'>
            {items.map((e) => (
              // <Cards key={e.Img} data={e}/>
              <div className='col-sm-2 col-md-4 col-lg-3 card border-0 mt-3' onClick={products}>
                <img src={e.Img} alt="images" className='w-100 rounded-3' />
                <div className='card-body'>
                  <p className='text-danger text-center'>{e.Name}</p>
                </div>



              </div>
            ))}
          </div>
        </div>
      </div>




      {/* Nursery and services */}
      <div className='mt-5' onClick={handleClick}>
        <h3 className='text-info text-center'>Nursery & Landscaping Services</h3>
        <p className='text-secondary text-center mx-5'>At Heartyculture, we not only provide a vibrant selection of nurtured plants but also offer expert landscaping services to transform your outdoor spaces into lush, personalized gardens.</p>
        <div className='container'>
          <div className='row mt-3'>
            <div className='col-lg-6 col-sm-2 bg-image'><img src='https://www.heartyculturenursery.com/cdn/shop/files/Nursery_Landscaping_Page_Images_for_Heartyculture_e41999a7-c3f0-41a2-a5ae-cd9f74c871dc_1350x760.jpg?v=1692023343' alt="" className='w-100 ' /></div>
            <div className='col-lg-6 col-sm-2'><img src='https://www.heartyculturenursery.com/cdn/shop/files/untitleddesign1_0px3_1350x760.jpg?v=1691415173' alt="" className='w-100' /></div>
          </div>

        </div>
      </div>


      <div className='container mt-5'>
        <h4 className='text-info text-center'>Plant Care Tips & Guidance</h4>
        <div className='row mt-3 w-auto'>
          <div className='col-sm-2 col-md-4 '><img src='https://www.heartyculturenursery.com/cdn/shop/articles/annie-spratt-gI_z1P5zC-M-unsplash_1167x500_crop_center.jpg?v=1617178865' alt='' className='w-100' /><p className='fs-4 text-center'>Container gardening</p><p className='text-center'>
            Container gardening is a soothing, practical and fun way to build opportunities and relationships with the plant kingdom. Bright and colorful points of interest are easy to create with planned spacing. A handy location near the kitchen can be a...</p></div>
          <div className='col-sm-2 col-md-4  '><img src='https://www.heartyculturenursery.com/cdn/shop/articles/pedro-kummel-xU1mshiwvfY-unsplash_1895x750_crop_center.jpg?v=1616648679' alt='' className='w-100' /><p className='fs-4 text-center'>Foundation for Plant Care</p><p className='text-center'>
            Once we have committed to our chosen plant spaces and plant selections, it is a gift to acknowledge that we now and maybe again, are local participants in the magic of photosynthesis and its Divine results. Regardless of the geographic...</p></div>
          <div className='col-sm-2 col-md-4  '><img src='https://www.heartyculturenursery.com/cdn/shop/articles/shelby-miller-dueAYSt07YU-unsplash_1895x750_crop_center.jpg?v=1616583028' alt='' className='w-100' /><p className='fs-4 text-center'>Plant Selection</p>
            <p className='text-center'> Before beginning a garden project, you may have several ideas in mind for enhancing spaces with plants and it is good to take things one a step at a time. Having seen a certain plant, a particular landscape or...</p></div>

        </div>
      </div>




      {/* video */}
      <div className='container'>
        <video>   <source src="https://www.youtube.com/watch?v=lLk_GKV8kxo" className="object-fit-cover w-100" autoPlay></source>
        </video>
      </div>
      <hr className='mx-5'/>

{/* Footer */}
      <div className='footer text-center text-warning'>
        <p>Search |Terms of Service | Refund policy</p>
        <p>Copyright © 2023 Heartyculture Nursery.</p>
        <p>Powered by Basha</p>
      </div>



    </div>
  )
}

export default Home