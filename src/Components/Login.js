
import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, Input, Label, FormGroup } from 'reactstrap';
import Register from './Register';
import { useNavigate } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';
import Home from './Home';


function Login(args) {
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  //  const navigate=useNavigate()
  const [formdata, setFormdata] = useState({
    email: '',
    password: ''
  })


  const handleInput = (e) => {
    const { name, value } = e.target

    setFormdata({
      ...formdata,
      [name]: value
    })

  }





  const toggle = (e) => {
    e.preventDefault();
    setModal(!modal)
  };






  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      let response = await axios.get(`http://localhost:3001/login/${formdata.email}/${formdata.password}`)
      console.log(response.data)
    } catch (err) {
      throw err
    }

    setLoading(false);
    alert('Login successfully');


  }




  return (
    <div>
      {/* <Button color="secondary" onClick={toggle}>
       Login
     </Button>     */}
      <h6 href='#' onClick={toggle} className=' text-dark'>Login</h6>


      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}><center><h3>Login</h3></center></ModalHeader>
        <ModalBody>
          <>
            <Form onSubmit={handleSubmit}>
              <FormGroup floating>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="Email"
                  type="email"
                  pattern="(?=.*\@)" title="Must contain '@' character"
                  value={formdata.email}
                  onChange={handleInput}
                  required


                />
                <Label for="exampleEmail">
                  Email
                </Label>
              </FormGroup>

              <FormGroup floating>
                <Input
                  id="examplePassword"
                  name="password"
                  placeholder="Password"
                  type="password"
                  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                  value={formdata.password}
                  onChange={handleInput}
                  required

                />
                <Label for="examplePassword">
                  Password
                </Label>
              </FormGroup>

              <Button type='submit' className='btn-success' disabled={loading} >

                {loading ?
                  <>
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                    &nbsp;Loading...
                  </> : 'Sign in'
                }
              </Button>
            </Form>
          </>
        </ModalBody>
        <ModalFooter>
          <p>New Customer?</p>
          <Register />
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}


export default Login;